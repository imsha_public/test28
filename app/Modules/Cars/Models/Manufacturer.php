<?php

namespace App\Modules\Cars\Models;

use App\Modules\Cars\Models\Factories\ManufacturerFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Модель производителя автомобиля
 *
 * @property integer $id
 * @property string $name
 */
class Manufacturer extends Model
{
    use HasFactory;
    public $timestamps = false;

    /**
     * @return ManufacturerFactory
     */
    protected static function newFactory(): ManufacturerFactory
    {
        return new ManufacturerFactory();
    }
}
