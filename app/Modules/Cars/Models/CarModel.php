<?php

namespace App\Modules\Cars\Models;

use App\Modules\Cars\Models\Factories\CarModelFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Модель модели автомобиля
 *
 * @property integer $id
 * @property string $name
 * @property Manufacturer $manufacturer
 */
class CarModel extends Model
{
    use HasFactory;
    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function manufacturer(): BelongsTo
    {
        return $this->belongsTo(Manufacturer::class, 'manufacturer_id');
    }

    /**
     * @return CarModelFactory
     */
    protected static function newFactory(): CarModelFactory
    {
        return new CarModelFactory();
    }
}
