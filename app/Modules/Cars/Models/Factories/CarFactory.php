<?php

namespace App\Modules\Cars\Models\Factories;

use App\Modules\Cars\Models\Car;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Modules\Cars\Models\Car>
 */
class CarFactory extends Factory
{
    protected $model = Car::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'production_year' => $this->faker->year,
            'mileage' => $this->faker->numberBetween(10000, 500000),
            'color' => $this->faker->colorName,
        ];
    }
}
