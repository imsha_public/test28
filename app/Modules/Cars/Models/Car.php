<?php

namespace App\Modules\Cars\Models;

use App\Modules\Cars\Models\Factories\CarFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Модель одного автомобиля
 *
 * @property int $id
 * @property CarModel $carModel
 * @property int $production_year
 * @property int $mileage
 * @property string $color
 */
class Car extends Model
{
    use HasFactory;

    /** @var string[]  */
    protected $fillable = [
        'car_model_id',
        'production_year',
        'mileage',
        'color',
    ];

    /** @var string[]  */
    protected $with = [
        'carModel.manufacturer'
    ];

    /**
     * @return BelongsTo
     */
    public function carModel(): BelongsTo
    {
        return $this->belongsTo(CarModel::class, 'car_model_id');
    }

    /**
     * @return CarFactory
     */
    protected static function newFactory(): CarFactory
    {
        return new CarFactory();
    }
}
