<?php

namespace App\Modules\Cars\Http\Requests;

use App\Modules\Cars\Services\Params\CarDeleteParams;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Запрос обновления автомобиля
 */
class CarDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    protected function getId(): int
    {
        return (int) $this->route('id');
    }

    /**
     * @return CarDeleteParams
     */
    public function toServiceParams(): CarDeleteParams
    {
        return new CarDeleteParams(
            id: $this->getId(),
        );
    }
}
