<?php

namespace App\Modules\Cars\Http\Requests;

use App\Modules\Cars\Services\Params\PaginationParams;
use Illuminate\Foundation\Http\FormRequest;

class PaginationRequest extends FormRequest
{
    private const DEFAULT_LIMIT = 100;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'limit' => [
                'integer',
                'min:0',
                'max:100',
            ],
            'offset' => [
                'integer',
                'min:0',
                'max:1000',
            ]
        ];
    }

    protected function getLimit(): int
    {
        return $this->get('limit', self::DEFAULT_LIMIT);
    }

    protected function getOffset(): int
    {
        return $this->get('offset', 0);
    }

    /**
     * @return PaginationParams
     */
    public function toServiceParams(): PaginationParams
    {
        return new PaginationParams(
            limit: $this->getLimit(),
            offset: $this->getOffset(),
        );
    }
}
