<?php

namespace App\Modules\Cars\Http\Requests;

use App\Modules\Cars\Models\CarModel;
use App\Modules\Cars\Services\Params\CarCreateParams;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Запрос создания автомобиля
 */
class CarCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(CarModel $carModel)
    {
        $productionYearMax = date('Y');

        return [
            'car_model_id' => [
                'required',
                'integer',
                Rule::exists($carModel->getTable(), $carModel->getQualifiedKeyName()),
            ],
            'production_year' => [
                'required',
                'integer',
                'min:1800',
                "max:{$productionYearMax}",
            ],
            'mileage' => [
                'required',
                'integer',
                'min:0',
                "max:50000000",
            ],
            'color' => [
                'required',
                'string',
                "min:1",
                "max:2000",
            ],
        ];
    }

    protected function carModelId(): int
    {
        return $this->get('car_model_id');
    }

    protected function productionYear(): int
    {
        return $this->get('production_year');
    }

    protected function mileage(): int
    {
        return $this->get('mileage');
    }

    protected function color(): string
    {
        return $this->get('color');
    }

    /**
     * @return CarCreateParams
     */
    public function toServiceParams(): CarCreateParams
    {
        return new CarCreateParams(
            carModelId: $this->carModelId(),
            productionYear: $this->productionYear(),
            mileage: $this->mileage(),
            color: $this->color(),
        );
    }
}
