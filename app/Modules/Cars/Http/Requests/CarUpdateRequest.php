<?php

namespace App\Modules\Cars\Http\Requests;

use App\Modules\Cars\Services\Params\CarUpdateParams;

/**
 * Запрос обновления автомобиля
 */
class CarUpdateRequest extends CarCreateRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    protected function getId(): int
    {
        return (int) $this->route('id');
    }

    /**
     * @return CarUpdateParams
     */
    public function toServiceParams(): CarUpdateParams
    {
        return new CarUpdateParams(
            carModelId: $this->carModelId(),
            productionYear: $this->productionYear(),
            mileage: $this->mileage(),
            color: $this->color(),
            id: $this->getId(),
        );
    }
}
