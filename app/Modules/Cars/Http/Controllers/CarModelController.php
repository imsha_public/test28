<?php

namespace App\Modules\Cars\Http\Controllers;

use App\Modules\Cars\Http\Requests\PaginationRequest;
use App\Modules\Cars\Services\CarModelListService;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class CarModelController extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    protected CarModelListService $listService;

    public function __construct(CarModelListService $listService)
    {
        $this->listService = $listService;
    }

    /**
     * Список производителей
     *
     * @param PaginationRequest $paginationRequest
     * @return LengthAwarePaginator
     */
    public function index(PaginationRequest $paginationRequest): LengthAwarePaginator
    {
        return $this->listService->list($paginationRequest->toServiceParams());
    }
}
