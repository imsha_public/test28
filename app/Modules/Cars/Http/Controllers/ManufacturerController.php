<?php

namespace App\Modules\Cars\Http\Controllers;

use App\Modules\Cars\Http\Requests\PaginationRequest;
use App\Modules\Cars\Services\ManufacturerListService;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * Производители
 */
class ManufacturerController extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    protected ManufacturerListService $listService;

    public function __construct(ManufacturerListService $listService)
    {
        $this->listService = $listService;
    }

    /**
     * Список производителей
     *
     * @param PaginationRequest $paginationRequest
     * @return LengthAwarePaginator
     */
    public function index(PaginationRequest $paginationRequest): LengthAwarePaginator
    {
        return $this->listService->list($paginationRequest->toServiceParams());
    }
}
