<?php

namespace App\Modules\Cars\Http\Controllers;

use App\Modules\Cars\Http\Requests\CarCreateRequest;
use App\Modules\Cars\Http\Requests\CarDeleteRequest;
use App\Modules\Cars\Http\Requests\CarUpdateRequest;
use App\Modules\Cars\Http\Requests\PaginationRequest;
use App\Modules\Cars\Http\Resources\CarResource;
use App\Modules\Cars\Models\Car;
use App\Modules\Cars\Services\CarCreateService;
use App\Modules\Cars\Services\CarDeleteService;
use App\Modules\Cars\Services\CarListService;
use App\Modules\Cars\Services\CarUpdateService;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class CarController extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    protected CarListService $listService;
    protected CarCreateService $createService;
    protected CarUpdateService $updateService;
    protected CarDeleteService $deleteService;

    public function __construct(
        CarListService   $listService,
        CarCreateService $createService,
        CarUpdateService $updateService,
        CarDeleteService $deleteService
    ) {
        $this->listService = $listService;
        $this->createService = $createService;
        $this->updateService = $updateService;
        $this->deleteService = $deleteService;
    }

    /**
     * Список производителей
     *
     * @see https://i.imgur.com/q9O99Ck.png
     *
     * @param PaginationRequest $paginationRequest
     * @return LengthAwarePaginator
     */
    public function index(PaginationRequest $paginationRequest): LengthAwarePaginator
    {
        return $this->listService->list($paginationRequest->toServiceParams());
    }

    /**
     * Просмотр одного автомобиля
     *
     * @param Car $car
     * @return CarResource
     */
    public function show(Car $car): CarResource
    {
        return CarResource::make($car);
    }

    /**
     * Создание одной записи об автомобиле
     *
     * @param CarCreateRequest $carCreateRequest
     * @return CarResource
     */
    public function store(CarCreateRequest $carCreateRequest): CarResource
    {
        return $this->createService->store($carCreateRequest->toServiceParams());
    }

    /**
     * Обновление одной записи
     *
     * @param CarUpdateRequest $carUpdateRequest
     * @return CarResource
     */
    public function update(CarUpdateRequest $carUpdateRequest): CarResource
    {
        return $this->updateService->update($carUpdateRequest->toServiceParams());
    }

    /**
     * Удаление автомобиля
     * @param CarDeleteRequest $deleteRequest
     * @return JsonResponse
     */
    public function destroy(CarDeleteRequest $deleteRequest): JsonResponse
    {
        return new JsonResponse([
            'success' => $this->deleteService->delete($deleteRequest->toServiceParams())
        ]);
    }
}
