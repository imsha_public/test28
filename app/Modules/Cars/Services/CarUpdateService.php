<?php

namespace App\Modules\Cars\Services;

use App\Modules\Cars\Http\Resources\CarResource;
use App\Modules\Cars\Models\Car;
use App\Modules\Cars\Services\Params\CarCreateParams;
use App\Modules\Cars\Services\Params\CarUpdateParams;

/**
 * Сервис для выборки списка моделей
 */
class CarUpdateService
{
    /** @var Car $carModel */
    protected Car $carModel;

    /**
     * @param Car $carModel
     */
    public function __construct(Car $carModel)
    {
        $this->carModel = $carModel;
    }

    public function update(CarUpdateParams $updateParams): CarResource
    {
        /** @var Car $model */
        $model = $this->carModel->newQuery()->findOrFail($updateParams->getId());
        $model->production_year = $updateParams->getProductionYear();
        $model->mileage = $updateParams->getMileage();
        $model->color = $updateParams->getColor();
        $model->carModel()->associate($updateParams->getCarModelId());
        $model->save();
        return CarResource::make($model);
    }
}
