<?php

namespace App\Modules\Cars\Services;

use App\Modules\Cars\Models\Car;
use App\Modules\Cars\Services\Params\PaginationParams;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Сервис для выборки списка моделей
 */
class CarListService implements ListServiceInterface
{
    /** @var Car $carModel */
    protected Car $carModel;

    /**
     * @param Car $carModel
     */
    public function __construct(Car $carModel)
    {
        $this->carModel = $carModel;
    }

    /**
     * @inheritdoc
     */
    public function list(PaginationParams $paginationParams): LengthAwarePaginator
    {
        return $this->carModel->newQuery()->paginate(
            perPage: $paginationParams->getLimit(),
            page: $paginationParams->getOffset(),
        );
    }
}
