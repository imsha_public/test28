<?php

namespace App\Modules\Cars\Services;

use App\Modules\Cars\Http\Resources\CarResource;
use App\Modules\Cars\Models\Car;
use App\Modules\Cars\Services\Params\CarDeleteParams;

/**
 * Сервис для удаления автомобиля
 */
class CarDeleteService
{
    /** @var Car $carModel */
    protected Car $carModel;

    /**
     * @param Car $carModel
     */
    public function __construct(Car $carModel)
    {
        $this->carModel = $carModel;
    }

    public function delete(CarDeleteParams $createParams): bool
    {
        return (bool) $this->carModel->newQuery()->findOrFail($createParams->getId())->delete();
    }
}
