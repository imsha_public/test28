<?php

namespace App\Modules\Cars\Services;

use App\Modules\Cars\Models\CarModel;
use App\Modules\Cars\Services\Params\PaginationParams;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Сервис для выборки списка моделей
 */
class CarModelListService implements ListServiceInterface
{
    /** @var CarModel $carModel */
    protected CarModel $carModel;

    /**
     * @param CarModel $carModel
     */
    public function __construct(CarModel $carModel)
    {
        $this->carModel = $carModel;
    }

    /**
     * @inheritdoc
     */
    public function list(PaginationParams $paginationParams): LengthAwarePaginator
    {
        return $this->carModel->newQuery()->paginate(
            perPage: $paginationParams->getLimit(),
            page: $paginationParams->getOffset(),
        );
    }
}
