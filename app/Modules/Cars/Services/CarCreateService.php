<?php

namespace App\Modules\Cars\Services;

use App\Modules\Cars\Http\Resources\CarResource;
use App\Modules\Cars\Models\Car;
use App\Modules\Cars\Services\Params\CarCreateParams;

/**
 * Сервис для выборки списка моделей
 */
class CarCreateService
{
    /** @var Car $carModel */
    protected Car $carModel;

    /**
     * @param Car $carModel
     */
    public function __construct(Car $carModel)
    {
        $this->carModel = $carModel;
    }

    public function store(CarCreateParams $createParams): CarResource
    {
        $model = clone $this->carModel;
        $model->production_year = $createParams->getProductionYear();
        $model->mileage = $createParams->getMileage();
        $model->color = $createParams->getColor();
        $model->carModel()->associate($createParams->getCarModelId());
        $model->save();
        return CarResource::make($model);
    }
}
