<?php

namespace App\Modules\Cars\Services;

use App\Modules\Cars\Models\Manufacturer;
use App\Modules\Cars\Services\Params\PaginationParams;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Сервис для выборки списка производителей
 */
class ManufacturerListService implements ListServiceInterface
{
    /** @var Manufacturer  */
    protected Manufacturer $manufacturerModel;

    /**
     * @param Manufacturer $manufacturerModel
     */
    public function __construct(Manufacturer $manufacturerModel)
    {
        $this->manufacturerModel = $manufacturerModel;
    }

    /**
     * @inheritdoc
     */
    public function list(PaginationParams $paginationParams): LengthAwarePaginator
    {
        return $this->manufacturerModel->newQuery()->paginate(
            perPage: $paginationParams->getLimit(),
            page: $paginationParams->getOffset(),
        );
    }
}
