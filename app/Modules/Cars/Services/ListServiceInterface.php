<?php

namespace App\Modules\Cars\Services;

use App\Modules\Cars\Services\Params\PaginationParams;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Интерфейс сервиса для выборки списка
 */
interface ListServiceInterface
{
    /**
     * @param PaginationParams $paginationParams
     * @return LengthAwarePaginator
     */
    public function list(PaginationParams $paginationParams): LengthAwarePaginator;
}
