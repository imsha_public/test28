<?php

namespace App\Modules\Cars\Services\Params;

/**
 * DTO класс параметров удаления автомобиля
 */
class CarDeleteParams
{
    private int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
