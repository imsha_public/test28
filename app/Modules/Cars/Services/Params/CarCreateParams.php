<?php

namespace App\Modules\Cars\Services\Params;

/**
 * DTO класс параметров  автомобиля
 */
class CarCreateParams
{
    protected int $carModelId;

    protected int $productionYear;

    protected int $mileage;

    protected string $color;

    /**
     * @param int $carModelId
     * @param int $productionYear
     * @param int $mileage
     * @param string $color
     */
    public function __construct(int $carModelId, int $productionYear, int $mileage, string $color)
    {
        $this->carModelId = $carModelId;
        $this->productionYear = $productionYear;
        $this->mileage = $mileage;
        $this->color = $color;
    }

    /**
     * @return int
     */
    public function getCarModelId(): int
    {
        return $this->carModelId;
    }

    /**
     * @return int
     */
    public function getProductionYear(): int
    {
        return $this->productionYear;
    }

    /**
     * @return int
     */
    public function getMileage(): int
    {
        return $this->mileage;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }
}
