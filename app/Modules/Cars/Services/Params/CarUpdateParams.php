<?php

namespace App\Modules\Cars\Services\Params;

/**
 * DTO класс параметров  автомобиля
 */
class CarUpdateParams extends CarCreateParams
{
    protected int $id;

    /**
     * @param int $id
     * @param int $carModelId
     * @param int $productionYear
     * @param int $mileage
     * @param string $color
     */
    public function __construct(int $carModelId, int $productionYear, int $mileage, string $color, int $id = 0)
    {
        parent::__construct(
            carModelId: $carModelId,
            productionYear: $productionYear,
            mileage: $mileage,
            color: $color,
        );

        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
