<?php

namespace App\Modules\Cars\Services\Params;

/**
 * Dto класс параметров погинации
 */
class PaginationParams
{
    protected int $limit = 100;

    protected int $offset = 0;

    public function __construct(int $limit, int $offset)
    {
        $this->limit = $limit;
        $this->offset = $offset;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }
}
