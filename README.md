Тестовое задание 28

Цель нашего тестового задания - это определить уровень ваших знаний и навыков по разработке бэкенда веб-приложения. Тестовое задание сформулировано без каких-то жёстких требований, но просим при реализации учитывать лучшую практику из вашего опыта по оформлению кода и репозитория, обеспечению безопасности, производительности и командной работе. Пожалуйста, помните, что код и API должны быть не только рабочими, но и стабильными, понятными и удобными в использовании.
Подготовка

Создайте новый репозиторий на Gitlab.com, его нужно назвать “test28”. Для приватного репозитория предоставьте, пожалуйста, доступ для пользователя “dataloft”.
Установите последнюю версию фреймворка Laravel (9.x).
Модели данных

Марка автомобиля
Название
Модель автомобиля
Название
Марка
Автомобиль
Марка
Модель
Год выпуска (опциональный атрибут)
Пробег (опциональный атрибут)
Цвет (опциональный атрибут)
API

Список марок автомобилей;
Список моделей автомобилей;
Список + CRUD автомобилей.
Дополнительное задание
(выполняется по желанию)

Привязать автомобиль к пользователю и разрешить пользователям доступ только к “своим” автомобилям.
