<?php

namespace Database\Seeders;

use App\Modules\Cars\Models\Car;
use App\Modules\Cars\Models\CarModel;

use App\Modules\Cars\Models\Manufacturer;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        Manufacturer::factory(10)->create()->each(function (Manufacturer $manufacturer) {
            CarModel::factory(10, [
                'manufacturer_id' => $manufacturer->id
            ])->create()->each(function (CarModel $carModel) {
                Car::factory(2, [
                    'car_model_id' => $carModel->id
                ])->create();
            });
        });
    }
}
