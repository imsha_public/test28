<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('car_model_id')->unsigned();
            $table->integer('production_year')->comment('Год выпуска автомобиля');
            $table->integer('mileage')->unsigned()->comment('Пробег в километрах');
            $table->string('color', 200)->comment('Цвет');

            $table->foreign('car_model_id')
                ->on('car_models')
                ->references('id')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('cars');
    }
};
