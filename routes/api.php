<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('v1')->group(function () {
    Route::get('manufacturers', [\App\Modules\Cars\Http\Controllers\ManufacturerController::class, 'index']);
    Route::get('models', [\App\Modules\Cars\Http\Controllers\CarModelController::class, 'index']);
    Route::get('cars', [\App\Modules\Cars\Http\Controllers\CarController::class, 'index']);
    Route::get('cars/{car}', [\App\Modules\Cars\Http\Controllers\CarController::class, 'show']);

    Route::post('cars', [\App\Modules\Cars\Http\Controllers\CarController::class, 'store']);
    Route::put('cars/{id}', [\App\Modules\Cars\Http\Controllers\CarController::class, 'update']);
    Route::delete('cars/{id}', [\App\Modules\Cars\Http\Controllers\CarController::class, 'destroy']);
});
